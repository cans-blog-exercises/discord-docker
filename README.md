# Discord Docker
An example of X11 application containerization with Discord and Docker.

## Build
Download `discord-<version>.deb` file from offical discord website and place under current directory as
`discord.deb`. Issue command `sudo make build`.

## Run
Issue command `sudo make run`.
