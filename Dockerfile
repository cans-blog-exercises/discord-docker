FROM debian:buster-slim

COPY discord.deb /tmp/discord.deb
RUN \
    apt-get update && \
    apt-get install --no-install-recommends --yes \
        /tmp/discord.deb && \
    rm /tmp/discord.deb

# Install missing dependencies.
RUN \
    apt-get install --no-install-recommends --yes \
        libx11-xcb1 \
        libxcb-dri3-0 \
        libatk-bridge2.0-0 \
        libgtk-3-0 \
        libdrm2 \
        libgbm1

# Install pulseaudio client library.
RUN \
    apt-get install --no-install-recommends --yes \
        libpulse0
