IMAGE_NAME=discord:runtime
CONTAINER_NAME=discord
HOST_USERNAME=can

.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .

.PHONY: run
run: build
	( \
		(docker rm -f $(CONTAINER_NAME) || true) && \
		pulseaudio_socket=$(shell mktemp --tmpdir=/tmp --suffix=.socket --dry-run pulseaudioXXX) && \
		su $(HOST_USERNAME) --command "XDG_RUNTIME_DIR=/run/user/$(shell id -u $(HOST_USERNAME)) \
			pactl load-module module-native-protocol-unix socket=$$pulseaudio_socket" && \
		docker run \
			--tty \
			--detach \
			--volume /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 \
			--device=/dev/dri:/dev/dri \
			--volume /home/can/.config/discord:/home/can/.config/discord \
			--volume "$$pulseaudio_socket":/tmp/pulseaudio.socket \
			--env DISPLAY=:0 \
			--env HOME=/home/can \
			--env PULSE_SERVER=unix:/tmp/pulseaudio.socket \
			--user 1000 \
			--name $(CONTAINER_NAME) \
			$(IMAGE_NAME) /usr/share/discord/Discord --no-sandbox --no-xshm --disable-dev-shm-usage \
	)
